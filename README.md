# Abacus [![build](https://gitlab.com/bscubed/abacus/badges/master/pipeline.svg)](https://gitlab.com/bscubed/abacus/pipelines/latest)

Abacus is an advanced, open-source calculator for Android. Written with :heart: in Kotlin with flexibility, customizability, and modularity in mind.

The goal of Abacus is to provide one calculator to rule them all. Graphing calculators are expensive, and the phone you carry in your pocket is around 20 times more powerful than the average calculator needed at most universities. And that's a conservative estimate. So why not utilize that power and get an app that does everything a typical graphing calculator can do and more?

As of right now there's not much to see, but feel free to stay tuned and watch this project grow into something beautiful. If you have a request, [submit one][request]!

### Planned Features
  - [x] Clean, modern UI
  - [x] Basic operators
  - [x] Advanced functions
      - [x] Square root
      - [x] Factorial
      - [x] Logarithms
      - [x] Constants (pi and euler's number)
      - [x] Trigonometric functions
  - [x] Order of operations
  - [x] Nested parenthesis
  - [x] Efficient, easy to customize, object-oriented solving algorithm
  - [x] Smart input
  - [ ] 2D (maybe 3D) graphing
      - [ ] Calculate zeroes
      - [ ] Minimum/maximum
      - [ ] Intersection
      - [ ] Table view
  - [ ] Algebra
      - [ ] Equation reduction
      - [ ] Polynomial solving
  - [ ] Statistics tools
      - [ ] Data sets
      - [ ] Linear regression
      - [ ] Quadratic regression
      - [ ] Data set manipulations
  - [ ] Linear algebra
      - [ ] Matrices
      - [ ] Reduced echelon form
      - [ ] Reduced row echelon form
      - [ ] Row operations
  - [ ] Calculus
      - [ ] Differentiation
      - [ ] Integration
  - [ ] Modular design
  - [ ] Customizeable panels

[request]: <https://gitlab.com/bscubed/abacus/issues>
