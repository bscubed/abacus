/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.utils

import android.util.Log
import com.opus.abacus.utils.functions.*
import java.util.concurrent.CopyOnWriteArrayList

/**
 * This class stores a math equation in the form of a list of math objects and provides a simple, customizable, and
 * readable algorithm for solving said equation.
 */
class Expression(val nodes: CopyOnWriteArrayList<Node> = CopyOnWriteArrayList()) {

    private val expressionChangedListeners: MutableList<OnExpressionChangedListener> = mutableListOf()

    val length: Int get() = nodes.size

    val last: Node get() = nodes[length - 1]

    /**
     * This algorithm adds a function or number to our expression. It checks for certain input values that are
     * impossible and attempts to fix them to minimize errors.
     */
    fun add(node: Node, index: Int = length) {
        var position = index

        if (isNotEmpty() && shouldMultiply(getPrevious(position), node)) {
            nodes.add(Multiply(true))
            position++
        }
        // If we're adding a number and the previous node is also a number, we append our number to the end of the
        // previous node's number.
        if (node is MutableNumber) {
            if (isNotEmpty() && getPrevious(position) is MutableNumber) {
                (nodes[position - 1] as MutableNumber).add(node)
                return
            } else if ((length > 1 && getPrevious(position) is Subtract && getPrevious(position - 1) !is Number && getPrevious(
                    position - 1
                ) !is RightParenthesis) || (length == 1 && getPrevious(
                    position
                ) is Subtract)
            ) {
                replace(getPrevious(position), MutableNumber("-" + node.display))
                return
            }
        }
        // If we're adding a decimal, we check if the last node is a number. If it is, we add the decimal to the number,
        // if it isn't, we add a number containing just a decimal to the expression.
        if (node is Decimal) {
            if (isNotEmpty() && getPrevious(position) is MutableNumber) {
                (nodes[position - 1] as MutableNumber).addDecimalIfNotExists()
            } else {
                add(MutableNumber("."), position)
            }
            return
        }
        if (node is Operator) {
            // Check all of the operators before the input and behind the last non-operator value,
            // if there is one, and react accordingly
            for (i in position - 1 downTo 0) {
                val currentNode = get(i)
                if (currentNode is Operator) {
                    if (node is Subtract) { // If the previous node is an plus or minus, flip the signs.
                        if (currentNode is Subtract) {
                            remove(i)
                            position--
                            nodes.add(position, Add())
                            return
                        } else if (currentNode is Add) {
                            remove(i)
                            position--
                            nodes.add(position, Subtract())
                            return
                        }
                    } else {
                        remove(i)
                        position--
                    }
                } else {
                    break
                }
            }
        }
        // If the node should have a parenthesis next to it, (Eg. log() ln() sin() sqrt() etc) add an opening parenthesis
        if (node is Function.Parenthetical) {
            // Here, we bind the node and it's parenthesis together so when one is deleted, the other is deleted as well
            val parenthesis = LeftParenthesis(node)
            node.parenthesisObject = parenthesis

            nodes.add(position, node)
            position++
            nodes.add(position, parenthesis)
            return
        }
        // If none of the previous cases are true, we just add the node.
        nodes.add(position, node)
    }

    /**
     * Deletes the function or number at the specified index.
     * Defaults to the last node in the expression.
     */
    fun backspace(index: Int = length - 1) {
        var position = index
        // Do nothing if the expression is empty
        if (isEmpty()) return

        val node = get(position)
        // If the node we're deleting is a number, we'll just delete the last digit of the number. If the number has a
        // length of one, we delete the object as well.
        if (node is MutableNumber) {
            if (node.length > 1) {
                node.backspace(this)
                return
            }
        }
        if (node is LeftParenthesis && node.relationship != null) {
            nodes.remove(node)
            position--
        }

        remove(position)
        position--

        // If there's a hidden node, automatically remove it.
        if (position >= 0 && get(position).hidden) {
            remove(position)
        }
    }

    fun clear() {
        nodes.clear()
    }

    // TODO: Build the best solving algorithm man has seen
    fun solve(expression: Expression): Expression {
        Log.i("Expression", "Solving: ${expression.getDebugString()}")

        // Base case: Solve the equation in order of operations: parenthesis, functions, exponents,
        // multiplication/division, and addition/subtraction
        if (!expression.contains<LeftParenthesis>()) {
            for (node in expression.nodes) {
                if (node is Function && node !is Exponent) {
                    node.solve(expression)
                }
            }
            for (node in expression.nodes) {
                if (node is Exponent) {
                    node.solve(expression)
                }
            }
            for (node in expression.nodes) {
                if (node is Multiply) {
                    node.solve(expression)
                } else if (node is Divide) {
                    node.solve(expression)
                }
            }
            for (node in expression.nodes) {
                if (node is Add) {
                    node.solve(expression)
                } else if (node is Subtract) {
                    node.solve(expression)
                }
            }

            return expression
        }

        // Solve each instance of a nested set of parenthesis recursively.
        var leftParenthesis: Int
        var rightParenthesis = expression.length - 1
        // Starts at the end of the array and moves left until it finds the last opening parenthesis.
        for (index in expression.nodes.size - 1 downTo 0) {
            if (expression.get(index) is LeftParenthesis) {
                leftParenthesis = index
                // Once it is found, we move to the right and find the matching closing parenthesis. (It will always be
                // the first one.)
                for (index2 in index until expression.nodes.size) {
                    if (expression.get(index2) is RightParenthesis) {
                        rightParenthesis = index2
                        break
                    }
                }
                // Solves the expression contained in the nested parenthesis and continues to search for the next
                // instance of nested parenthesis.
                val subExpression = expression.getSubExpression(leftParenthesis, rightParenthesis)
                subExpression.removeParenthesis()
                val solvedSubExpression = solve(subExpression)
                expression.replace(leftParenthesis, rightParenthesis, solvedSubExpression)
            }
        }

        // Solves one last time at the base level. There should be no parenthesis left here.
        return solve(expression)
    }

    inline fun <reified T : Node> contains(): Boolean {
        for (node in nodes) {
            if (node is T) return true
        }
        return false
    }

    fun getPosition(node: Node) = nodes.indexOf(node)

    fun get(index: Int) = nodes[index]

    fun getPrevious(index: Int) = nodes[index - 1]

    fun remove(index: Int) {
        nodes.removeAt(index)
    }

    fun removeAll(start: Int, end: Int) {
        for (index in start..end) {
            remove(start)
        }
    }

    // Replaces a node in our expression with another node
    fun replace(oldNode: Node, newNode: Node) {
        val index = nodes.indexOf(oldNode)
        nodes.removeAt(index)
        nodes.add(index, newNode)
    }

    fun replace(start: Int, end: Int, node: Node) {
        removeAll(start, end)
        nodes.add(start, node)
    }

    fun replace(start: Int, end: Int, expression: Expression) {
        removeAll(start, end)
        nodes.addAll(start, expression.nodes)
    }

    fun isEmpty() = length == 0

    fun isNotEmpty() = length > 0

    fun getSubExpression(start: Int, end: Int): Expression =
        Expression(CopyOnWriteArrayList(nodes.subList(start, end + 1)))

    fun removeParenthesis() {
        for (node in nodes) {
            if (node is LeftParenthesis || node is RightParenthesis) nodes.remove(node)
        }
    }

    fun notifyExpressionChanged() {
        expressionChanged()
    }

    fun addOnExpressionChangedListener(onExpressionChangedListener: OnExpressionChangedListener) {
        expressionChangedListeners.add(onExpressionChangedListener)
    }

    // Whether or not we should add an implicit multiplicand. For example, 3log(5) = 3*log(5)
    private fun shouldMultiply(lastNode: Node, currentNode: Node): Boolean {
        if (currentNode is Function.Multiplicative) {
            if (currentNode is MutableNumber && lastNode is MutableNumber) return false
            return when (lastNode) {
                is Number -> true
                is RightParenthesis -> true
                else -> false
            }
        } else {
            return false
        }
    }

    // Signals all objects implementing [OnExpressionChangedListener] that the expression has changed and a new value
    // can be displayed
    private fun expressionChanged() {
        for (expressionChangedListener in expressionChangedListeners) {
            expressionChangedListener.onExpressionChanged(this)
        }
    }

    // Returns all parts of our expression, visible or not, and puts a space in-between each part in order to
    // differentiate between each part.
    fun getDebugString(): String {
        var string = ""
        for (node in nodes) {
            string += "${node.display} "
        }
        return string
    }

    override fun toString(): String {
        var string = ""
        for (node in nodes) {
            if (!node.hidden) {
                string += node.display
            }
        }
        return string
    }

    interface OnExpressionChangedListener {
        fun onExpressionChanged(expression: Expression)
    }
}