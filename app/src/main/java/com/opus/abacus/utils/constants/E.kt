/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.utils.constants

import com.opus.abacus.utils.Constant

class E(override var hidden: Boolean = false) : Constant() {
    override var display = "e"
    override val value = 2.718281828459045235360287471352662497757247093699959574966967627724076630353
}