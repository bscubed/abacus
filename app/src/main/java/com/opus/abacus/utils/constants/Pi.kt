/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.utils.constants

import com.opus.abacus.utils.Constant

class Pi(override var hidden: Boolean = false) : Constant() {
    override var display = "π"
    override val value = 3.141592653589793238462643383279502884197169399375105820974944592307816406286
}