/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.utils.functions

import com.opus.abacus.utils.Expression
import com.opus.abacus.utils.Function
import com.opus.abacus.utils.Node

class LeftParenthesis(var relationship: Node? = null, override var hidden: Boolean = false) : Node(), Function,
    Function.Multiplicative {

    override var display: String = "("

    override fun solve(expression: Expression) {

    }
}