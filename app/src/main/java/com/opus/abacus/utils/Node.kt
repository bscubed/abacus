/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.utils

/**
 * This class is used as the parent class of all objects that are used in storing and solving our expression.
 * It allows us to store a list of objects that extend this class, regardless of type.
 * Honestly, I wish I could have thought of a better name for this class than Node but I mean... what else
 * is there? Part? Too generic. ExpressionObject? Too long. I dunno, Node works I guess.
 */
abstract class Node {
    // The string that is displayed in the editor for the specific node.
    abstract var display: String
    // Whether or not this node should be displayed in the editor. This is useful for supplying hidden operators
    // in our expression. For example, when a number is next to a constant, (3π) it's implied the number is multiplied
    // by the constant (3 * π). This allows us to put a hidden multiplication symbol in our expression and waste less
    // resources looking for instances of multiplication when solving.
    abstract var hidden: Boolean

    fun previous(expression: Expression): Node {
        val position = expression.getPosition(this)
        return expression.get(position - 1)
    }

    fun next(expression: Expression): Node {
        val position = expression.getPosition(this)
        return expression.get(position + 1)
    }
}