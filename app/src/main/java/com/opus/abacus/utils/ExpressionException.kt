/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.utils

/**
 * The base class for any exception caused by an error in the solve algorithm.
 *
 * @param display: The error text to show on the active display.
 */
abstract class ExpressionException(val display: String = DEFAULT_ERROR_TEXT) : Exception() {
    companion object {
        const val DEFAULT_ERROR_TEXT = "Syntax error"
    }
}