/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.utils.functions

import com.opus.abacus.utils.*
import com.opus.abacus.utils.Function
import com.opus.abacus.utils.Number

class Factorial(override var hidden: Boolean = false) : Node(), Function {

    override var display: String = "!"

    override fun solve(expression: Expression) {
        val number = previous(expression) as Number
        val solvedValue = factorial(number.value.toLong())
        val answerNode = MutableNumber(solvedValue.toString())
        answerNode.reformat()
        val end = expression.nodes.indexOf(this)
        val start = end - 1
        expression.replace(start, end, answerNode)
    }

    private fun factorial(input: Long): Long {
        return if (input >= 1) input * factorial(input - 1)
        else 1
    }
}