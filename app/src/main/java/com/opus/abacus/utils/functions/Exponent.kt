/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.utils.functions

import com.opus.abacus.utils.*
import com.opus.abacus.utils.Function
import com.opus.abacus.utils.Number
import kotlin.math.pow

class Exponent(override var hidden: Boolean = false) : Node(), Function {

    override var display: String = "^"

    override fun solve(expression: Expression) {
        val previousNumber = previous(expression) as Number
        val nextNumber = next(expression) as Number
        val solvedValue = (previousNumber.value.pow(nextNumber.value))
        val answerNode = MutableNumber(solvedValue.toString())
        answerNode.reformat()
        val start = expression.nodes.indexOf(previousNumber)
        val end = expression.nodes.indexOf(nextNumber)
        expression.replace(start, end, answerNode)
    }
}