/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.utils

import com.opus.abacus.utils.functions.Subtract
import java.text.DecimalFormat
import java.text.NumberFormat

class MutableNumber(override var display: String, override var hidden: Boolean = false) : Number() {

    val length: Int
        get() {
            return when {
                placeholder != null -> 1
                else -> display.length
            }
        }

    private var placeholder: String? = null

    override val value: Double
        get() {
            return when (display) {
                "." -> 0.0
                "Infinity" -> Double.POSITIVE_INFINITY
                "∞" -> Double.POSITIVE_INFINITY
                "NaN" -> Double.NaN
                else -> NumberFormat.getInstance().parse(display).toDouble()
            }
        }
    // override val value: Double get() = NumberFormat.getInstance().parse(if (display == ".") "0" else display).toDouble()

    fun addDecimalIfNotExists() {
        if (placeholder != null) resetPlaceholder()
        if (display.contains('.')) return
        display += '.'
    }

    fun add(number: Number) {
        if (placeholder != null) resetPlaceholder()
        display += number.display
        // If our number has a decimal, we don't need to worry about reformatting the display value
        if (display.contains('.')) return
        reformat()
    }

    fun backspace(expression: Expression) {
        display = display.removeRange(length - 1, length)
        if (display.contains('.')) return
        if (display == "-") {
            expression.replace(this, Subtract())
            return
        }
        reformat()
    }

    fun reformat() {
        when {
            value.isInfinite() -> {
                placeholder = "∞"
                display = placeholder!!
            }
            value.isNaN() -> {
                placeholder = "NaN"
                display = placeholder!!
            }
            else -> display = DecimalFormat("#,###.##########").format(value)
        }
    }

    private fun resetPlaceholder() {
        display = ""
        placeholder = null
    }
}