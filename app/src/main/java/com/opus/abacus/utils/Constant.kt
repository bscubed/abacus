/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.utils

/**
 * The parent class for all constants. (Pi, Euler's number, etc.)
 * @property value: The value this constant represents.
 * @property display: The string to show in the editor display.
 * @property hidden: Whether or not this node should be shown in the editor.
 */
abstract class Constant : Number()