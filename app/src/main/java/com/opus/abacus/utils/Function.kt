/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.utils

import com.opus.abacus.utils.functions.LeftParenthesis

/**
 * This is the parent class for any node that operates on a number or other node. The provided [solve] function allows
 * easy customization of each function's solving algorithm.
 */
interface Function {
    fun solve(expression: Expression)

    // A class can implement this interface if, when combined with another number or constant, an implicit
    // multiplicand should be added in-between. For example, 3log(5) is equivalent to 3*log(5).
    interface Multiplicative

    interface Parenthetical {
        var parenthesisObject: LeftParenthesis?
    }
}