/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.utils.functions

import com.opus.abacus.utils.*
import com.opus.abacus.utils.Function
import com.opus.abacus.utils.Number
import kotlin.math.tan

class Tan(override var hidden: Boolean = false) : Node(), Function, Function.Parenthetical, Function.Multiplicative {

    override var parenthesisObject: LeftParenthesis? = null

    override var display: String = "tan"

    override fun solve(expression: Expression) {
        val number = next(expression) as Number
        val solvedValue = tan(number.value)
        val answerNode = MutableNumber(solvedValue.toString())
        answerNode.reformat()
        val start = expression.nodes.indexOf(this)
        val end = start + 1
        expression.replace(start, end, answerNode)
    }
}