/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.data

import androidx.lifecycle.ViewModel
import com.opus.abacus.utils.Expression

class CalculatorViewModel : ViewModel() {
    var expression = Expression()
}