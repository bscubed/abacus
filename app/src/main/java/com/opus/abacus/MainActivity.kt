/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.navigation.NavigationView
import com.opus.abacus.data.CalculatorViewModel
import com.opus.abacus.fragments.CalculatorFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    var currentFragment: Fragment? = null

    lateinit var viewModel: CalculatorViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nav_view.setNavigationItemSelectedListener(this)

        viewModel = ViewModelProviders.of(this).get(CalculatorViewModel::class.java)

        // TODO: Once we have multiple fragments, we need to remember to set the selected item to the current fragment here
        // Sets the default page to the first navigation drawer menu item.
        onNavigationItemSelected(nav_view.menu.getItem(0))
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_calculator -> {
                currentFragment = Fragment.instantiate(
                    this@MainActivity,
                    CalculatorFragment::class.java.name
                ) as CalculatorFragment
            }
            R.id.nav_about -> {

            }
            R.id.nav_settings -> {

            }
        }

        if (currentFragment != null) {
            supportFragmentManager.beginTransaction().replace(
                R.id.content_placeholder,
                currentFragment!!, currentFragment!!::class.java.simpleName
            ).commit()
            if (item.isCheckable) {
                item.isChecked = true
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
