/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.opus.abacus.R
import com.opus.abacus.data.CalculatorViewModel
import com.opus.abacus.utils.Expression
import com.opus.abacus.views.DisplayEditText

/**
 * This Fragment sets up the layout and functionality for the main calculator
 */
class CalculatorFragment : Fragment() {

    lateinit var expression: Expression
    lateinit var viewModel: CalculatorViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val viewGroup = inflater.inflate(R.layout.content_calculator, container, false) as ViewGroup
        val display = viewGroup.findViewById<DisplayEditText>(R.id.display)

        // Gets our Activity ViewModel and restores our layout data from it.
        viewModel = activity?.run {
            ViewModelProviders.of(this).get(CalculatorViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        expression = viewModel.expression
        display.expression = expression

        // Sets our display as the receiver of all calculator input
        expression.addOnExpressionChangedListener(display)
        expression.notifyExpressionChanged()

        return viewGroup
    }
}
