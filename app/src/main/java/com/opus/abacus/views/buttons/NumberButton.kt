/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.views.buttons

import android.content.Context
import android.util.AttributeSet
import com.opus.abacus.R
import com.opus.abacus.utils.MutableNumber
import com.opus.abacus.utils.Node
import com.opus.abacus.views.InputButton

/**
 * This view extends the functionality of the normal button and allows us to define a value to be directly added to our
 * expression to solve.
 */
class NumberButton : InputButton {

    private var value: Int = 0

    constructor(context: Context) : super(context) {
        init(context, null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        init(context, attrs, defStyle)
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyle: Int) {
        // Get custom XML tags from attrs.xml
        val a = context.obtainStyledAttributes(attrs, R.styleable.NumberButton, defStyle, 0)
        value = a.getInt(R.styleable.NumberButton_value, value)
        a.recycle()

        text = value.toString()
    }

    override fun getNode(): Node {
        return MutableNumber(text.toString())
    }
}