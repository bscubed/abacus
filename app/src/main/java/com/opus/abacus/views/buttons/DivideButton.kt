/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.views.buttons

import android.content.Context
import android.util.AttributeSet
import com.opus.abacus.utils.Node
import com.opus.abacus.utils.functions.Divide
import com.opus.abacus.views.InputButton

class DivideButton : InputButton {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    override fun getNode(): Node {
        return Divide()
    }
}