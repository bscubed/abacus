/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.views

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton
import com.opus.abacus.MainActivity
import com.opus.abacus.fragments.CalculatorFragment
import com.opus.abacus.utils.Node
import com.opus.abacus.views.buttons.NumberButton

/**
 * This is an abstract class designed to provide common inheritance to any button that adds an input to the expression.
 */
abstract class InputButton : AppCompatButton {

    constructor(context: Context) : super(context) {
        setOnClickListener()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        setOnClickListener()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        setOnClickListener()
    }

    // Set the OnClickListener to add the desired node to our expression.
    private fun setOnClickListener() {
        setOnClickListener {
            ((context as MainActivity).currentFragment as CalculatorFragment).expression.add(getNode())
            ((context as MainActivity).currentFragment as CalculatorFragment).expression.notifyExpressionChanged()
        }
        // TODO: Remove this once we're done with our temporary functions implementation.
        if (this !is NumberButton) {
            text = getNode().display
        }
    }

    abstract fun getNode(): Node
}