/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.views.buttons

import android.content.Context
import android.util.AttributeSet
import com.opus.abacus.MainActivity
import com.opus.abacus.fragments.CalculatorFragment
import com.opus.abacus.utils.ExpressionException
import com.opus.abacus.views.ActionButton
import kotlinx.android.synthetic.main.content_calculator.*
import kotlin.concurrent.thread

class EqualsButton : ActionButton {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    override fun performAction() {
        val fragment = (context as MainActivity).currentFragment as CalculatorFragment
        val expression = fragment.expression
        thread {
            try {
                fragment.expression = expression.solve(expression)
            } catch (exception: Exception) {
                fragment.display.showError(
                    if (exception is ExpressionException) {
                        exception.display
                    } else {
                        ExpressionException.DEFAULT_ERROR_TEXT
                    }
                )
                return@thread
            }
            (context as MainActivity).runOnUiThread {
                fragment.expression.notifyExpressionChanged()
            }
        }
    }
}