/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.opus.abacus.R

/**
 * This layout simply allows you to specify the desired amount of rows and columns, and it will
 * automatically arrange the children of this layout in an organized grid. This is useful for our
 * calculator layout, which is mostly grid-based.
 */
class PadLayout : ViewGroup {
    // The number of columns and rows, respectively.
    private var numColumns = resources.getInteger(R.integer.default_grid_layout_width)
    private var numRows = resources.getInteger(R.integer.default_grid_layout_height)

    // Whether the rows are rendered from top to bottom or bottom to top. Default is top to bottom.
    private var orientation = ORIENTATION_TOP

    var rowHeight: Int? = null

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        init(attrs, defStyle)
    }

    /**
     * Gets custom XML attributes from attrs.xml
     */
    private fun init(attrs: AttributeSet?, defStyle: Int) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.PadLayout, defStyle, 0)

        numColumns = a.getInteger(R.styleable.PadLayout_columns, numColumns)
        numRows = a.getInteger(R.styleable.PadLayout_rows, numRows)
        orientation = a.getInteger(R.styleable.PadLayout_orientation, orientation)

        a.recycle()
    }

    override fun shouldDelayChildPressedState(): Boolean {
        return false
    }

    /**
     * This method is called when this Layout is rendered. Its purpose is to layout the children in a grid pattern.
     */
    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        val paddingLeft = paddingLeft
        val paddingRight = paddingRight
        val paddingTop = paddingTop
        val paddingBottom = paddingBottom

        val isRTL = layoutDirection == View.LAYOUT_DIRECTION_RTL
        val columnWidth = Math.round((right - left - paddingLeft - paddingRight).toFloat()) / numColumns
        val rowHeight = if (rowHeight == null) {
            Math.round((bottom - top - paddingTop - paddingBottom).toFloat()) / numRows
        } else {
            rowHeight!!
        }

        var rowIndex = 0
        var columnIndex = 0

        // Cycle through all children views and set the layouts to be confined to the relative column and row index.
        for (childIndex in 0 until childCount) {
            val childView = getChildAt(childIndex)

            // Do nothing if the child is hidden.
            if (childView.visibility == View.GONE) {
                continue
            }

            val lp = childView.layoutParams as ViewGroup.MarginLayoutParams

            // Determine the child's new layout parameters
            var childTop: Int
            var childBottom: Int
            if (orientation == ORIENTATION_TOP) {
                childTop = paddingTop + lp.topMargin + rowIndex * rowHeight
                childBottom = childTop - lp.topMargin - lp.bottomMargin + rowHeight
            } else {
                childBottom = bottom - rowIndex * rowHeight
                childTop = childBottom - rowHeight
            }
            val childLeft = paddingLeft + lp.leftMargin +
                    (if (isRTL) numColumns - 1 - columnIndex else columnIndex) * columnWidth
            val childRight = childLeft - lp.leftMargin - lp.rightMargin + columnWidth

            val childWidth = childRight - childLeft
            val childHeight = childBottom - childTop
            if (childWidth != childView.measuredWidth || childHeight != childView.measuredHeight) {
                childView.measure(
                    View.MeasureSpec.makeMeasureSpec(childWidth, View.MeasureSpec.EXACTLY),
                    View.MeasureSpec.makeMeasureSpec(childHeight, View.MeasureSpec.EXACTLY))
            }
            childView.layout(childLeft, childTop, childRight, childBottom)

            // Get the row and column index for the next child, if there is one.
            if (childIndex != childCount) {
                rowIndex += (columnIndex + 1) / numColumns
                columnIndex = (columnIndex + 1) % numColumns
            }
        }
    }

    override fun generateLayoutParams(attrs: AttributeSet): FrameLayout.LayoutParams {
        return FrameLayout.LayoutParams(context, attrs)
    }

    override fun generateDefaultLayoutParams(): FrameLayout.LayoutParams {
        return FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun generateLayoutParams(p: ViewGroup.LayoutParams): ViewGroup.LayoutParams {
        return ViewGroup.MarginLayoutParams(p)
    }

    override fun checkLayoutParams(p: ViewGroup.LayoutParams): Boolean {
        return p is ViewGroup.MarginLayoutParams
    }

    companion object {
        const val ORIENTATION_TOP = 0
        const val ORIENTATION_BOTTOM = 1
    }
}