/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.views.buttons

import android.content.Context
import android.util.AttributeSet
import com.opus.abacus.MainActivity
import com.opus.abacus.fragments.CalculatorFragment
import com.opus.abacus.views.ActionButton

class DeleteButton : ActionButton {

    init {
        setOnLongClickListener {
            ((context as MainActivity).currentFragment as CalculatorFragment).expression.clear()
            ((context as MainActivity).currentFragment as CalculatorFragment).expression.notifyExpressionChanged()
            true
        }
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    override fun performAction() {
        ((context as MainActivity).currentFragment as CalculatorFragment).expression.backspace()
        ((context as MainActivity).currentFragment as CalculatorFragment).expression.notifyExpressionChanged()
    }
}