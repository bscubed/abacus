/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.views

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageButton

/**
 * This is an abstract class designed to provide common inheritance to any button that performs an action on click.
 */
abstract class ActionButton : AppCompatImageButton {

    constructor(context: Context) : super(context) {
        setOnClickListener()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        setOnClickListener()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        setOnClickListener()
    }

    // Set the OnClickListener to add the desired node to our expression.
    private fun setOnClickListener() {
        setOnClickListener {
            performAction()
        }
    }

    abstract fun performAction()
}