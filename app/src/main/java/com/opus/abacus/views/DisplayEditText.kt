/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.views

import android.content.Context
import android.graphics.Rect
import android.os.Parcelable
import android.text.SpannableStringBuilder
import android.text.method.ScrollingMovementMethod
import android.util.AttributeSet
import android.view.View
import android.widget.EditText
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import com.opus.abacus.R
import com.opus.abacus.utils.Expression

/**
 * A custom view that's based off of [EditText], designed as a way to display and edit an [Expression] object.
 */
class DisplayEditText @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) :
    AppCompatEditText(context, attrs, defStyle), Expression.OnExpressionChangedListener {

    var expression: Expression? = null

    // Temporary object for use in layout methods.
    private val mTempRect = Rect()

    private var mWidthConstraint = -1

    init {
        if (isFocusable) {
            movementMethod = ScrollingMovementMethod.getInstance()
        }
        minHeight = lineHeight + compoundPaddingBottom + compoundPaddingTop
        showSoftInputOnFocus = false
        requestFocus()
    }

    fun showError(display: String) {
        text = SpannableStringBuilder(display)
        setTextColor(ContextCompat.getColor(context, R.color.displayErrorTextColor))
    }

    // This snaps the selection to equation nodes instead of each individual character
    private fun getSelectionIndex(): Int {
        if (selectionStart != selectionEnd) return length()
        val index = selectionEnd
        var newIndex = 0
        if (expression != null) {
            for (node in expression!!.nodes) {
                newIndex += node.display.length
                if (newIndex > index && (newIndex - index).toDouble() / (node.display.length).toDouble() > 0.5) {
                    newIndex -= node.display.length
                    break
                }
            }
        }
        return newIndex
    }

    private fun getEquationIndex(): Int {
        if (selectionStart != selectionEnd) return expression!!.length
        val index = selectionEnd
        var newIndex = 0
        var equationIndex = 0
        for (node in expression!!.nodes) {
            newIndex += node.display.length
            equationIndex++
            if (newIndex > index) break
        }
        return equationIndex
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        mWidthConstraint = View.MeasureSpec.getSize(widthMeasureSpec) - paddingLeft - paddingRight
    }

    override fun onSaveInstanceState(): Parcelable? {
        super.onSaveInstanceState()

        // EditText will freeze any text with a selection regardless of getFreezesText() ->
        // return null to prevent any state from being preserved at the instance level.
        return null
    }

    override fun getCompoundPaddingTop(): Int {
        // Measure the top padding from the capital letter height of the text instead of the top,
        // but don't remove more than the available top padding otherwise clipping may occur.
        paint.getTextBounds("H", 0, 1, mTempRect)

        val fontMetrics = paint.fontMetricsInt
        val paddingOffset = -(fontMetrics.ascent + mTempRect.height())
        return super.getCompoundPaddingTop() - Math.min(paddingTop, paddingOffset)
    }

    override fun getCompoundPaddingBottom(): Int {
        // Measure the bottom padding from the baseline of the text instead of the bottom, but don't
        // remove more than the available bottom padding otherwise clipping may occur.
        val fontMetrics = paint.fontMetricsInt
        return super.getCompoundPaddingBottom() - Math.min(paddingBottom, fontMetrics.descent)
    }

    override fun onTextChanged(text: CharSequence?, start: Int, lengthBefore: Int, lengthAfter: Int) {
        setSelection(lengthAfter)
    }

    override fun onSelectionChanged(selStart: Int, selEnd: Int) {
        // TODO: Add equation insertion at selected index. Until then, disable selection.
        setSelection(length())
        //setSelection(getSelectionIndex())
    }

    override fun onExpressionChanged(expression: Expression) {
        setTextColor(ContextCompat.getColor(context, R.color.displayTextColor))
        text = SpannableStringBuilder(expression.toString())
    }
}
