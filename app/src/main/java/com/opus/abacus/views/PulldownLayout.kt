/*
 * Copyright (c) 2018 Abacus Project
 * Licensed under GPLv3 or any later version
 * Refer to the LICENSE file included in the root directory
 */

package com.opus.abacus.views

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.view.ViewCompat
import androidx.customview.widget.ViewDragHelper
import com.opus.abacus.R

/**
 * This layout creates a view with a visible handle that can be pulled down to reveal hidden content.
 * The handle, when pulled down, extends to the bottom of the current layout.
 * The handle view and content view are automatically sized to fit [getMeasuredHeight] and
 * [verticalRange] respectively.
 */
class PulldownLayout(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {

    private var handleViewId = -1
    private var contentViewId = -1

    private var pulldownView: View? = null
    private var handleView: View? = null
    private var contentView: View? = null

    private var viewDragHelper: ViewDragHelper? = null

    // The offset of the pulldown when expanded.
    private var max: Int = 0
    // The offset of the pulldown when collapsed.
    private var min: Int = 0
    // The height of the PulldownLayout in pixels.
    private var pulldownHeight: Int = 0
    // The current offset of the pulldown. -verticalRange when collapsed, 0 when expanded.
    private var topOffset: Int = 0
    // The sliding range of the pulldown in pixels.
    private var verticalRange: Int = 0

    // The current state of the viewDragHelper object. Defaults to being idle.
    private var draggingState = ViewDragHelper.STATE_IDLE
    private var state: Int = CLOSED

    private val stateChangeListeners: MutableList<StateChangedListener> = mutableListOf()

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.PulldownLayout)
        handleViewId = a.getResourceId(R.styleable.PulldownLayout_handle, handleViewId)
        contentViewId = a.getResourceId(R.styleable.PulldownLayout_content, contentViewId)
        a.recycle()

        if (handleViewId == -1) {
            throw java.lang.IllegalStateException("PulldownLayout must provide a handle view")
        }
        if (contentViewId == -1) {
            throw java.lang.IllegalStateException("PulldownLayout must provide a content view")
        }

        viewDragHelper = ViewDragHelper.create(this, 1f, DragHelperCallback())
        elevation = 1f
    }

    override fun addView(child: View) {
        if (childCount > 0) {
            throw IllegalStateException("PulldownLayout can host only one direct child")
        }

        super.addView(child)
    }

    override fun addView(child: View, index: Int) {
        if (childCount > 0) {
            throw IllegalStateException("PulldownLayout can host only one direct child")
        }

        super.addView(child, index)
    }

    override fun addView(child: View, params: ViewGroup.LayoutParams) {
        if (childCount > 0) {
            throw IllegalStateException("PulldownLayout can host only one direct child")
        }

        super.addView(child, params)
    }

    override fun addView(child: View, index: Int, params: ViewGroup.LayoutParams) {
        if (childCount > 0) {
            throw IllegalStateException("PulldownLayout can host only one direct child")
        }

        super.addView(child, index, params)
    }

    override fun onFinishInflate() {
        pulldownView = getChildAt(0)
        handleView = findViewById(handleViewId)
        contentView = findViewById(contentViewId)

        super.onFinishInflate()
    }

    /**
     * Returns whether or not the user is currently touching the pulldown or not.
     * Useful when determining whether [viewDragHelper] should intercept the touch event ir not.
     */
    private fun isTarget(event: MotionEvent): Boolean {
        val target = IntArray(2)
        getLocationOnScreen(target)
        val lowerLimit = target[1]
        handleView!!.getLocationOnScreen(target)
        val upperLimit = target[1] + handleView!!.measuredHeight
        val y = event.rawY.toInt()
        return y in (lowerLimit + 1)..(upperLimit - 1)
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return isTarget(event) && viewDragHelper!!.shouldInterceptTouchEvent(event)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (isTarget(event) || isMoving()) {
            viewDragHelper!!.processTouchEvent(event)
            true
        } else {
            super.onTouchEvent(event)
        }
    }

    override fun computeScroll() { // needed for automatic settling.
        if (viewDragHelper!!.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this)
        }
    }

    /**
     * Overrides the layout of the children of this view and positions them to where they can be
     * interacted with by the user.
     */
    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        pulldownHeight = (parent as ViewGroup).measuredHeight - top
        verticalRange = pulldownHeight - measuredHeight
        bottom = (parent as ViewGroup).measuredHeight // Allows our view to extend to the bottom of the screen.

        // If our content contains a GridLayout, we set the height of each row to match the height of our handle.
        var extraFunctionsLayout: PadLayout? = null
        if (contentView is PadLayout) extraFunctionsLayout = contentView as PadLayout
        if (contentView is ViewGroup && (contentView as ViewGroup).getChildAt(0) is PadLayout) {
            extraFunctionsLayout = (contentView as ViewGroup).getChildAt(0) as PadLayout
        }
        if (extraFunctionsLayout != null) {
            extraFunctionsLayout.rowHeight = measuredHeight
        }

        pulldownView!!.layout(
            0,
            topOffset - verticalRange,
            r,
            topOffset + measuredHeight
        )

        handleView!!.layout(
            0,
            verticalRange,
            r,
            pulldownHeight
        )

        contentView!!.layout(
            0,
            0,
            r,
            verticalRange
        )
    }

    private fun notifyStateChanged(state: Int) {
        // Does nothing if the state hasn't changed
        if (this.state == state) return
        for (listener in stateChangeListeners) {
            listener.onStateChanged(state)
        }
    }

    fun addOnStateChangedListener(listener: StateChangedListener) {
        stateChangeListeners.add(listener)
    }

    fun isMoving(): Boolean {
        return draggingState == ViewDragHelper.STATE_DRAGGING || draggingState == ViewDragHelper.STATE_SETTLING
    }

    fun expand() {
        if (viewDragHelper!!.smoothSlideViewTo(pulldownView!!, 0, max)) {
            ViewCompat.postInvalidateOnAnimation(this)
        }
        notifyStateChanged(OPEN)
        state = OPEN
    }

    fun collapse() {
        if (viewDragHelper!!.smoothSlideViewTo(pulldownView!!, 0, min)) {
            ViewCompat.postInvalidateOnAnimation(this)
        }
        notifyStateChanged(CLOSED)
        state = CLOSED
    }

    /**
     * This class provides functionality to a [ViewDragHelper] instance.
     */
    private inner class DragHelperCallback : ViewDragHelper.Callback() {

        override fun onViewDragStateChanged(state: Int) {
            if (state == draggingState) {
                // Nothing has changed.
                return
            }
            if ((draggingState == ViewDragHelper.STATE_DRAGGING || draggingState == ViewDragHelper.STATE_SETTLING) && state == ViewDragHelper.STATE_IDLE) {
                // The pulldown has stopped moving.
                if (topOffset == min) {
                    // The view has finished collapsing.
                    notifyStateChanged(CLOSED)
                    this@PulldownLayout.state = CLOSED
                } else if (topOffset == max) {
                    // The view has finished expanding.
                    notifyStateChanged(CLOSED)
                    this@PulldownLayout.state = OPEN
                }
            }
            if (state == ViewDragHelper.STATE_DRAGGING) {
                // The view has started moving. Might need this later and don't feel like figuring how to get here
                // again.
            }
            draggingState = state
        }

        /**
         * This function is executed whenever the pulldown's position is changed by the user or
         * any other functionality.
         */
        override fun onViewPositionChanged(changedView: View, left: Int, top: Int, dx: Int, dy: Int) {
            topOffset = top - verticalRange
        }

        override fun getViewVerticalDragRange(child: View): Int {
            return verticalRange
        }

        override fun tryCaptureView(view: View, i: Int): Boolean {
            return view === pulldownView
        }

        /**
         * Forces our pulldown to move vertically.
         */
        override fun clampViewPositionVertical(child: View, top: Int, dy: Int): Int {
            min = paddingTop - verticalRange
            max = paddingTop
            return Math.min(Math.max(top, min), max)
        }

        /**
         * When the user lets go, calculate whether the pulldown should collapse or expand.
         */
        override fun onViewReleased(releasedChild: View, xvel: Float, yvel: Float) {
            if (topOffset == min) {
                notifyStateChanged(CLOSED)
                state = CLOSED
                return
            }
            if (topOffset == max) {
                notifyStateChanged(OPEN)
                state = OPEN
                return
            }

            val rangeToCheck = verticalRange.toFloat()
            var shouldExpand = false
            // If the fling velocity is high enough, settle at the position of the fling. Otherwise,
            // settle based on the pulldown offset.
            when {
                yvel > FLING_VELOCITY -> shouldExpand = true
                yvel < -FLING_VELOCITY -> shouldExpand = false
                topOffset > rangeToCheck / 2 -> shouldExpand = true
                topOffset < rangeToCheck / 2 -> shouldExpand = false
            }

            val settleDestY = if (shouldExpand) max else min

            if (viewDragHelper!!.settleCapturedViewAt(0, settleDestY)) {
                ViewCompat.postInvalidateOnAnimation(this@PulldownLayout)
            }
        }
    }

    interface StateChangedListener {
        fun onStateChanged(state: Int)
    }

    companion object {
        const val FLING_VELOCITY = 800.0
        const val OPEN = 0
        const val CLOSED = 1
    }
}